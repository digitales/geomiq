<?php
/**
 *
 * Search client
 *
 */
require __DIR__.'/vendor/autoload.php';

$input = '';

$fileHandler = new \App\Helpers\FileHandler();
if ($fileHandler->exists('input1.txt')) {
    $input = $fileHandler->getContents('input1.txt');

    if (is_array($input)) {
        $input = $input[0];// Only use the first row of the data
    }
}

$inputHandler = new \App\Helpers\InputHandler();

$processedData = $inputHandler->process($input);

// Json output

echo (string) $processedData;

// Create and start new batch
//$searchTerms = new \App\SearchTerms();
//$searchTerms->processAttributes($argv, __FILE__);
//
//$vendorList = new \App\VendorList();
//$vendorList->processAttributes($argv, __FILE__);
//$vendorList->processFile();
//
//$search = new \App\Search();
//$search->setVendorList($vendorList);
//$search->setSearchTerms($searchTerms);
//$search->setCurrentDatetime('10/11/18 00:00');// Hardcoded date for example CLI execution

//echo (string) $search->find();
