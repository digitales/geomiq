# Geomiq coding test

Author: Ross Tweedie <ross@nthdesigns.co.uk>

## Installation
1. Unzip the files.
2. Navigate to the unzipped files
3. Run `composer update` for PHPUnit
4. Run `php client.php`

## Requirements
To run the code PHP 7.xx is required.

Tested up to PHP 7.3.2 CLI



## Part A - Extension
To run as an API endpoint, I would install something like Laravel Lumen, and then follow the example in the client.php.

## Part A - Additional work
Given more time I would have fully implemented the validation classes to ensure the data via the input was valid for
the application.


# Part B

`SELECT
    u.id,
    u.name,
    COALESCE(
        (
            SELECT
                r.name FROM model_has_roles AS mhr
                JOIN roles AS r ON r.id = mhr.role_id
            WHERE
                mhr.model_id = u.id
                and mhr.model_type = 'App\\Models\\User'
         ),
        'buyer'
    ) AS `role`,
    u.email,
    p.company_name,
    u.created_at AS registered_on,
    u.last_login
    FROM
        users AS u
        JOIN profiles AS p ON p.user_id = u.id
    ORDER BY
        u.last_login DESC;`


## Extension
For the extension, I would create an event to generate the output of the query,