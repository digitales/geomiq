<?php
namespace Tests\Unit;

use App\Model\Feature;
use PHPUnit\Framework\TestCase;

/**
 * Class FeatureTest
 * @package Tests\Unit
 */
class FeatureTest extends TestCase
{
    protected $feature;

    protected function setUp(): void
    {
        $this->feature = new Feature();
        parent::setUp();
    }

    /** @test */
    public function canSetPosition()
    {
        $this->assertEquals([], $this->feature->position);

        $this->feature->position = [10];
        $this->assertEquals(1, $this->feature->countPositions);

        $this->feature->position = [10, 20, 30];
        $this->assertEquals(3, $this->feature->countPositions);

        $this->assertArrayHasKey(0, $this->feature->position);
    }

    /** @test */
    public function canSetDirection()
    {
        $this->assertEquals(null, $this->feature->direction);

        $this->feature->direction = 10;

        $this->assertEquals(10, $this->feature->direction);
    }

    /** @test */
    public function canSetRadius()
    {

        $this->assertEquals(null, $this->feature->radius);

        $this->feature->radius = 10;

        $this->assertEquals(10, $this->feature->radius);

    }

}
