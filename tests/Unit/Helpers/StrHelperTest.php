<?php
namespace Tests\Unit\Helpers;

use App\Helpers\StrHelper;
use PHPUnit\Framework\TestCase;

/**
 * Class StrHelperTest
 * @package Tests\Unit
 */
class StrHelperTest extends TestCase
{
    protected $helper;

    protected function setUp(): void
    {
        $this->helper = new StrHelper();
        parent::setUp();
    }

    /**
     * @test
     */
    public function canConvertToCamelCase()
    {
        $this->assertEquals('aTestMethodName', $this->helper::camel('a test method name'));
        $this->assertEquals('testMethodName', $this->helper::camel('test method name'));
    }


    /**
     * @test
     */
    public function canConvertToStudlyCase()
    {
        $this->assertEquals('ATestMethodName', $this->helper::studly('a test method name'));
        $this->assertEquals('TestMethodName', $this->helper::studly('test method name'));
    }

}
