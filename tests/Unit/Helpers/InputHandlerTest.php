<?php
namespace Tests\Unit\Helpers;

use App\Helpers\InputHandler;
use PHPUnit\Framework\TestCase;

/**
 * Class FeatureTest
 * @package Tests\Unit
 */
class InputHandlerTest extends TestCase
{
    protected $inputHandler;

    protected function setUp(): void
    {
        $this->inputHandler = new InputHandler();
        parent::setUp();
    }

    /**
     * @test
     */
    public function canExtractElapsedTime()
    {
        $result = $this->inputHandler->extractMeaning('elapsed_time=0.0022132396697998047');

        $this->assertEquals(null, $result->featureKey);
        $this->assertEquals('elapsed_time', $result->name);
        $this->assertEquals('0.0022132396697998047', $result->value);

    }

    /**
     * @test
     */
    public function canExtractPosition()
    {
        $result = $this->inputHandler->extractMeaning('position-1=0.000000000000014');

        $this->assertEquals(1, $result->featureKey);
        $this->assertEquals('position', $result->name);
        $this->assertEquals('0.000000000000014', $result->value);
//
        $result = $this->inputHandler->extractMeaning('position-1//90');

        $this->assertEquals(1, $result->featureKey);
        $this->assertEquals('position', $result->name);
        $this->assertEquals('90', $result->value[2]);
        $this->assertEquals(3, count($result->value));

    }

    /**
     * @test
     */
    public function canExtractDirection()
    {
        $result = $this->inputHandler->extractMeaning('direction-1=-2.0816681711721685e-16');

        $this->assertEquals(1, $result->featureKey);
        $this->assertEquals('direction', $result->name);
        $this->assertEquals('-2.0816681711721685e-16', $result->value);

        $result2 = $this->inputHandler->extractMeaning('direction-2=2.0816681711721685e-16');

        $this->assertEquals(2, $result2->featureKey);
        $this->assertEquals('direction', $result2->name);
        $this->assertEquals('2.0816681711721685e-16', $result2->value);
    }


    /**
     * @test
     */
    public function canExtractType()
    {
        $result = $this->inputHandler->extractMeaning(' type-CNC');
var_dump($result);
        $this->assertEquals(null, $result->featureKey);
        $this->assertEquals('type', $result->name);
        $this->assertEquals('CNC', $result->value);
    }

    /**
     * @test
     */
    public function canExtractRadius()
    {
        $result = $this->inputHandler->extractMeaning('radius-1-15');

        $this->assertEquals(1, $result->featureKey);
        $this->assertEquals('radius', $result->name);
        $this->assertEquals('15', $result->value);
    }


}
