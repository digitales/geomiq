<?php

namespace App\Model;

use App\Exceptions\AttributeMissingException;
use App\Helpers\StrHelper;

//use App\Exceptions\AttributeNotFoundException;

class ModelAbstract{

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        $methodName = StrHelper::camel( 'set ' . $name);

        // Now try for a mutator
        if (method_exists($this, $methodName)) {
            return $this->{$methodName}($value);
        }

        if (isset($this->{$name})) {
            $this->{$name} = $value;
            return $this;
        }


        return $this;
    }

    /**
     * Get the value of the attribute
     *
     * @param $name
     * @return mixed
     * @throws AttributeMissingException
     */
    public function __get($name)
    {

        $methodName = StrHelper::camel('get ' . $name);
        // Now try for a mutator
        if (method_exists($this, $methodName)) {
            return $this->{$methodName}();
        }

        if (isset($this->{$name})) {
            return $this->{$name};
        }

        throw new AttributeMissingException();
    }

}
