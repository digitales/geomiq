<?php
namespace App\Model;

class Feature extends ModelAbstract
{
    /**
     * @var string $id
     */
    public $id;

    /**
     * @var array The position
     */
    public $position = [];

    /**
     * @var float $direction the direction
     */
    public $direction;

    /**
     * @var int $radius The raduis
     */
    public $radius;

    /**
     * @return false|string
     */
    public function __toString()
    {
        $output = [
            'id' => (string) $this->id,
            'radius' => (int) $this->radius,
            'direction' => $this->direction,
            'position' => $this->position
        ];

        return json_encode($output);
    }

    /**
     * @return int
     */
    public function getCountPositions() : int
    {
        return count($this->position);
    }

    /**
     * @return array
     */
    public function output() : array
    {
        $toReturn = [];
        $toReturn['id'] = (string) $this->id;


        if ($this->radius) {
            $toReturn['radius'] = $this->radius;
        }

        if ($this->direction){
            $toReturn['direction'] = $this->direction;
        }

        if (null !== $this->position){
            $toReturn['position'] = (array)$this->position;
        }

        return $toReturn;
    }

}
