<?php
namespace App\Model;

class Job extends ModelAbstract
{

    public $features = [];

    public $elapsedTime;

    public $type;

    /**
     * @return int
     */
    public function getFeatureCount(): int
    {
        return (int) count($this->features);
    }

    public function hasFeature($key)
    {
        return isset($this->features[$key]);
    }


    /**
     * @param FeatureList $featureList
     * @return $this
     */
    public function setFeatureList(FeatureList $featureList)
    {
        $this->features = $featureList;

        return $this;
    }

    /**
     * @param Feature $feature
     * @return $this
     */
    public function setFeature(Feature $feature, int $key = null)
    {
        $this->features[$key] = $feature;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {

        // Process the features and remove the null attributes.
        $features = [];
        if (count($this->features) > 0) {
            foreach($this->features as $feature){
                $features[] = $feature->output();
            }
        }

        $toReturn = [
            'data' => $features,
            'elapsed_time' => $this->elapsedTime,
            'type' => $this->type,
            'feature_count' => count($this->features)
        ];


        var_dump($toReturn);


        var_dump($this);


        return json_encode($toReturn);
    }



}