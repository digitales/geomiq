<?php
namespace App\Helpers;

use App\Exceptions\FileNotFoundException;

/**
 * Class FileHandler
 *
 * Used to load the data from a given file.
 *
 * @package App\Helpers
 */
class FileHandler
{
    protected $rootDirectory;

    protected $filePath;

    /**
     * FileHandler constructor.
     * @param null $rootDirectory
     */
    public function __construct($rootDirectory = null)
    {
        $this->rootDirectory = $rootDirectory?: sprintf('storage%1$sdata%1$s', DIRECTORY_SEPARATOR);
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setFile(string $path) : self
    {
        if (!$this->exists($path)) {
            return $this;
        }

        if (dirname($path) == '.') {
            $this->filePath = $this->rootDirectory . $path;

            return $this;
        }

        $this->filePath = $path;

        return $this;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function exists(string $path)
    {
        if (file_exists($path)){
            return true;
        }

        // Check if the path is just a filename
        if (dirname($path) == '.') {
            $path = $this->rootDirectory . $path;
            if (file_exists($path)){
                $this->filePath = $path;
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function isDirectory(string $path) : bool
    {
        return is_dir($path);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function isFile(string $path) : bool
    {
        return is_file($path);
    }

    /**
     * @param string|null $filePath
     * @return array
     * @throws FileNotFoundException
     */
    public function getContents(string $filePath = null) : array
    {
        $toReturn = [];

        $filePath = $filePath?: $this->filePath;

        if (!$this->exists($filePath)) {
            throw new FileNotFoundException($filePath);
        }

        $fileHandler = fopen($this->filePath, 'r');

        while (!feof($fileHandler)) {
            $toReturn[] = trim(fgets($fileHandler));
        }
        fclose($fileHandler);

        return $toReturn;
    }


}

