<?php
namespace App\Helpers;

use App\Model\Feature;
use App\Model\Job;

class InputHandler
{
    /**
     * @var array the accepted part names allowed in the input.
     */
    protected $acceptedPartNames = [
        'id' => ['name' => 'id', 'type' => 'feature', 'cast' => 'string', 'hasKey' => true],
        'radius' => ['name' => 'radius', 'type' => 'feature', 'cast' => 'int', 'hasKey' => true],
        'direction' => ['name' => 'direction', 'type' => 'feature', 'cast' => 'string', 'hasKey' => true],
        'position' => ['name' => 'position', 'type' => 'feature', 'cast' => 'int', 'hasKey' => true],
        'elapsed_time' => ['name' => 'elapsed_time', 'type' => 'job', 'cast' => 'float', 'hasKey' => false],
        'type' => ['name' => 'type', 'type' => 'job', 'cast' => 'string', 'hasKey' => false],
    ];

    /**
     * @var array The possible delimiters
     */
    protected $delimiters = [
        '=',
        '-',
        '/',
    ];

    /**
     * @param string $input
     * @return Job
     */
    public function process(string $input) : Job
    {
        $terms = str_replace('%', ',', $input);
        $exploded = explode(',', $terms);

        $job = new Job();

        foreach ($exploded as $row) {
            foreach ($this->acceptedPartNames as $name => $params) {
                if (strpos($row, $name) !== false) {
                    $value = str_replace([$name, ' '], '', $row);

                    $key = true == $params['hasKey']? $this->extractKey($value) : null;

                    $extractedValue = $this->extractValue($value, $key);//$value);

                    $castType = isset($params['cast'])? $params['cast'] : null;

                    $valueToAdd = $this->castValue($extractedValue, $castType);

                    $variableName = StrHelper::camel($name);

                    if ('job' === $params['type']) {
                        $job->{$variableName} = $valueToAdd;
                        continue;
                    }

                    if (!$job->hasFeature($key)) {
                        $job->setFeature(
                            new Feature(),
                            $key
                        );
                        $job->features[$key]->id = $key;
                    }

                    $job->features[$key]->{$variableName} = $valueToAdd;
                }
            }
        }

        return $job;
    }

    /**
     * Get the details of the part
     *
     * @param $data
     * @return mixed
     */
    public function getPartDetails($data)
    {
        foreach ($this->acceptedPartNames as $name => $params) {
            if (strpos($data, $name) !== false) {
                return $params;
            }
        }
    }

    /**
     * Extract the meaning from the string
     *
     * @param string $data
     * @return \stdClass
     */
    public function extractMeaning(string $data) : \stdClass
    {
        $toReturn = new \stdClass();
        $toReturn->name = null;
        $toReturn->value = null;
        $toReturn->featureKey = null;

        $partDetails = $this->getPartDetails($data);

        $value = str_replace([$partDetails['name'], ' '], '', $data);

        $toReturn->name = $partDetails['name'];
        $toReturn->featureKey =  true == $partDetails['hasKey']? $this->extractKey($value) : null;
        $toReturn->value = $this->extractValue($value, $toReturn->featureKey);

        return $toReturn;
    }


    /**
     * Cast the value
     *
     * @param $value
     * @param string $cast
     * @return mixed
     */
    protected function castValue($value, $cast = 'string')
    {
        if (is_array($value)) {
            foreach ($value as $key => $row) {
                settype($value[$key], $cast);
            }

            return $value;
        }

        settype($value, $cast);

        return $value;
    }


    /**
     * @param string $value
     * @param string|null $key
     * @param array $delimiters
     * @return array|string
     */
    public function extractValue(string $value, ?string $key = null, array $delimiters = [])
    {
        $delimiters = $delimiters?: $this->delimiters;

        if ($key && strpos($value, '-' . $key, 0) !== false) {
            $value = substr_replace($value, '', 0, strlen('-' . $key));
        }

        $firstCharacter = substr($value, 0, 1);

        if (in_array($firstCharacter, $delimiters)) {
            $activeDelimiter = $delimiters[array_search($firstCharacter, $delimiters)];
            // If the delimiter appears multiple times, return as an array
            if (substr_count($value, $activeDelimiter) > 2) {
                return $this->extractArray($value, $activeDelimiter);
            }

            return str_replace($activeDelimiter, '', $value);
        }

        return '';
    }

    /**
     * Extract the key from the string
     *
     * @param string $value
     * @param string $keyDelimiter
     * @return string
     */
    public function extractKey(string $value, $keyDelimiter = '-'): string
    {
        $toReturn = '';
        $parts = explode($keyDelimiter, $value);

        // If the first element is empty, assume the string does not have a key.
        if (count($parts) < 2) {
            return $toReturn;
        }

        $toReturn = $parts[1];

        //Assume the second part is the key.
        foreach ($this->delimiters as $delimiter) {
            if (strpos($toReturn, $delimiter) !== false) {
                $exploded =  explode($delimiter, $toReturn);

                if (count($exploded) > 1) {
                    return $exploded[0];
                }
            }
        }

        return $toReturn;
    }

    /**
     * Extract an array from the string
     *
     * @param string $value
     * @param string $delimiter
     * @return array
     */
    public function extractArray(string $value, $delimiter = '/') : array
    {
        $toReturn = explode($delimiter, $value);

        foreach ($toReturn as $key => $row) {
            $toReturn[$key] = $this->castValue($row, 'int');
        }

        return $toReturn;
    }
}
