<?php
namespace App\Exceptions;

/**
 * Class AttributeMissingException
 * @package App\Exceptions
 */
class AttributeMissingException extends \Exception
{
}
