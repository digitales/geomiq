<?php
namespace App\Exceptions;

/**
 * Class AttributeNotFoundException
 * @package App\Exceptions
 */
class AttributeNotFoundException extends \Exception
{
}
