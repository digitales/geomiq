<?php
namespace App\Exceptions;

/**
 * Class FileNotFoundException
 * @package App\Exceptions
 * @since 1.0.0
 * @author Ross Tweedie <ross@nthdesigns.co.uk>
 */
class FileNotFoundException extends \Exception
{
}
