<?php
namespace App\Validation;

abstract class ValidationAbstract{

    var $rules = [];

    var $message;


    public function validate($input): bool
    {
        foreach($this->rules as $rule)
        {
            $result = preg_match($rule,$input);

            if (true == $result) {
                return true;
            }
        }

        return false;
    }

    public function getDataType()
    {
        return str_replace('Validation', '', (new \ReflectionClass($this))->getShortName());
    }

}
