<?php
namespace App\Validation;

class DirectionValidation extends ValidationAbstract implements ValidationInterface
{
    public $rules = ['/\d*/'];
}