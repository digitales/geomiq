<?php
namespace App\Validation;

/**
 * Interface ValidationInterface
 * @package App\Validation
 */
interface ValidationInterface{

    function validate($input) : bool;

}
