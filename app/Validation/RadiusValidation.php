<?php
namespace App\Validation;

//use App\Validation\ValidationAbstract;
//use App\Validation\ValidationInterface;

/**
 * Class RadiusValidation
 * @package App\Validation
 */
class RadiusValidation extends ValidationAbstract implements ValidationInterface
{
    public $rules = ['/\d*/'];
}
