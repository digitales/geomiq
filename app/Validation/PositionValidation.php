<?php
namespace App\Validation;

class PositionValidation extends ValidationAbstract implements ValidationInterface
{
    public $rules = ['/\d*/'];
}